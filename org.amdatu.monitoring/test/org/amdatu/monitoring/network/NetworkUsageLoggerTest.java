/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Dictionary;
import java.util.Hashtable;

import junit.framework.TestCase;

public class NetworkUsageLoggerTest extends TestCase {

    public void testNetworkUsageCounter() throws Exception {
        NetworkUsageLogger logger = new NetworkUsageLogger();
        NetworkUsageLogger logger2 = new NetworkUsageLogger();
        
        logger2.start();
        logger.start();

        Dictionary<String, String> result = new Hashtable<String, String>();
        result.put("type", "2");
        result.put("traffic-in", "0");
        result.put("traffic-out", "0");
        result.put("connections-server", "0");
        result.put("connections-client", "0");
        assert logger.call().equals(result);

        final ServerSocket s = new ServerSocket(8899);
        Thread t = new Thread() {
            public void run() {
                try {
                    Socket ss = s.accept();
                    InputStream in = ss.getInputStream();
                    int ch = 0;
                    for (int i = in.read(); i != -1; i = in.read()) {
                        if ((ch == '\n') && (i == '\n')) {
                            break;
                        }
                        if (i != '\r') {
                            ch = i;
                        }
                    }
                    OutputStream out = ss.getOutputStream();
                    out.write("HTTP/1.1 200 OK\r\n".getBytes());
                    out.write("Content-Type: text/xml;charset=utf-8\r\n".getBytes());
                    out.write("\r\n".getBytes());
                    out.close();
                    ss.close();

                }
                catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    try {
                        s.close();
                    }
                    catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }
        };
        t.start();
        
        long count = 0;

        URL url = new URL("http://127.0.0.1:8899/foo");
        InputStream in = url.openStream();
        try {
            while (in.read() != -1) {
                count++;
            }
        }
        finally {
            in.close();
        }
        
        try {
            t.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        System.out.println("COUNT = " + count);

        result.put("traffic-in", "212");
        result.put("traffic-out", "213");
        result.put("connections-server", "1");
        result.put("connections-client", "1");
        assert logger.call().equals(result);

        try {
            Socket.setSocketImplFactory(null);
            throw new IllegalStateException();
        }
        catch (Exception ex) {
            // pass
        }
        
        try {
            ServerSocket.setSocketFactory(null);
            throw new IllegalStateException();
        }
        catch (Exception ex) {
            // pass
        }
        
        logger.stop();
        
        try {
            Socket.setSocketImplFactory(null);
            throw new IllegalStateException();
        }
        catch (Exception ex) {
            // pass
        }
        
        try {
            ServerSocket.setSocketFactory(null);
            throw new IllegalStateException();
        }
        catch (Exception ex) {
            // pass
        }
        
        logger2.stop();
        
        Socket.setSocketImplFactory(null);
        ServerSocket.setSocketFactory(null);
    }
}
