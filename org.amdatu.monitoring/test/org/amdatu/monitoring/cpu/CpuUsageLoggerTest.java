/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.cpu;

import junit.framework.TestCase;

public class CpuUsageLoggerTest extends TestCase {
    
    public void testCpuUsageLogger() {
        final CpuUsageLogger logger = new CpuUsageLogger();
        Thread[] t = new Thread[] { new Thread() {
            public void run() {
                while (true) {
                    System.out.println(logger.call());
                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {
                        break;
                    }
                }
            }
        }, null, null, null, null };
        t[0].start();

        for (int i = 1; i < t.length; i++) {
            t[i] = new Thread() {
                public void run() {
                    for (int i = 0; i < 10; i++) {
                        int count = 1;
                        for (int j = 0; j < 20000000; j++) {
                            count += j / (0.5 * j);
                        }
                        // The print out is needed as otherwise the jit removes the
                        // complete computation :-D
                        System.out.println("---" + count);
                    }
                }
            };
            t[i].start();
        }
        try {
            for (int i = 1; i < t.length; i++) {
                t[i].join();
            }
            t[0].interrupt();
            t[0].join();
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
