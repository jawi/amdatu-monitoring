/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.disk;

import java.io.File;
import java.util.Dictionary;
import java.util.Properties;

import junit.framework.TestCase;

import org.osgi.service.cm.ConfigurationException;

public class DiskUsageLoggerTest extends TestCase {

    public void testDiskUsage() {
        DiskUsageLogger logger = new DiskUsageLogger();
        Dictionary<String, Object> result = logger.call();
        Properties props = new Properties();
        StringBuilder builder = new StringBuilder();
        for (File file : File.listRoots()) {
            builder.append(file.getPath()).append(File.pathSeparator);
        }
        builder.setLength(builder.length() - 1);
        props.put("roots", builder.toString());
        try {
            logger.updated(props);
        }
        catch (ConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        assert result.equals(logger.call());
    }
}
