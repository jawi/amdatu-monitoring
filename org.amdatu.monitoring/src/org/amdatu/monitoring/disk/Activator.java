/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.disk;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ManagedService;

/**
 * Publish the disk usage service.
 */
public class Activator implements BundleActivator {
    /** The configuration PID to use to configure this probe. */
    public static final String PID = "org.amdatu.monitoring.disk";

    @Override
    public void start(BundleContext context) throws Exception {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(org.osgi.framework.Constants.SERVICE_PID, PID);
        props.put(Constants.KEY_PROBE_NAME, Constants.DISK_MONITOR_NAME);
        props.put(Constants.KEY_PROBE_TYPE, Constants.DISK_MONITOR_TYPE);
        
        String[] ifaces = { Probe.class.getName(), ManagedService.class.getName() };

        context.registerService(ifaces, new DiskUsageLogger(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // Nop...
    }
}
