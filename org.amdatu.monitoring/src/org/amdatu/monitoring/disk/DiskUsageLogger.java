/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.disk;

import java.io.File;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * A disk usage monitor. See the {@link Constants} class for the kind of values it measures.
 * <p>
 * By default, the File.listRoots() are used as measured partitions. However, a configuration
 * can be used to change the partitions to a File.pathSeparator separated list of partition
 * path's.
 * </p>
 */
public class DiskUsageLogger implements Probe, ManagedService {
    public static final String ROOTS = "roots";

    private volatile File[] m_partitions = File.listRoots();

    /**
     * @see MonitoringEvent
     */
    public Dictionary<String, Object> call() {
        File[] partitions;
        synchronized (this) {
            partitions = m_partitions;
        }

        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.KEY_PROBE_TYPE, Constants.DISK_MONITOR_TYPE);

        for (File target : partitions) {
            long total = target.getTotalSpace();
            long free = target.getFreeSpace();

            String name = target.getAbsolutePath();
            if ("".equals(name)) {
                name = "/";
            }

            props.put(name + Constants.DISK_TOTAL_SUFFIX, total);
            props.put(name + Constants.DISK_FREE_SUFFIX, free);
            props.put(name + Constants.DISK_PERCENTAGE_SUFFIX, (long) ((1.0 - (((double) free) / total)) * 100));
        }
        return props;
    }

    /**
     * Called to update the configuration of this monitor.
     * 
     * @param properties if the {@link #ROOTS} key is pointing to a File.pathSeparator separated list of file path's these are used as the partitions to measure else
     *        File.listRoots() is used.
     * @see ConfigurationAdmin
     */
    public void updated(Dictionary properties) throws ConfigurationException {
        File[] partitions = getMonitoredPartitions(properties);
        synchronized (this) {
            m_partitions = partitions;
        }
    }

    /**
     * Returns the paritions that should be monitored according to the given properties.
     * 
     * @param properties the properties that contain the information about the to-be-
     *        monitored partitions. Can be <code>null</code> in which case all
     *        partitions are monitored.
     * @return an array with partitions, as {@link File} objects.
     */
    private File[] getMonitoredPartitions(Dictionary properties) {
        File[] partitions;
        if (properties != null) {
            List<File> list = new ArrayList<File>();

            String roots = (String) properties.get(ROOTS);
            if (roots != null) {
                for (String root : roots.split(File.pathSeparator)) {
                    File file = new File(root.trim());
                    if (file.exists()) {
                        list.add(file);
                    }
                }
            }

            partitions = list.toArray(new File[list.size()]);
        }
        else {
            partitions = File.listRoots();
        }

        return partitions;
    }
}
