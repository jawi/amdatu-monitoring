/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.channels.spi.AbstractSelectionKey;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class SelectorWrapper extends AbstractSelector {
    private final AbstractSelector m_as;
    final Map<SelectionKey, SelectionKey> m_keys =
        new HashMap<SelectionKey, SelectionKey>();

    SelectorWrapper(SelectorProvider provider, AbstractSelector as) {
        super(provider);
        m_as = as;
    }

    @Override
    public Selector wakeup() {
        clean();
        m_as.wakeup();
        clean();
        return this;
    }

    @Override
    public Set<SelectionKey> selectedKeys() {
        clean();
        return get(m_as.selectedKeys());
    }

    @Override
    public int selectNow() throws IOException {
        clean();
        try {
            return m_as.selectNow();

        }
        finally {
            clean();
        }
    }

    @Override
    public int select(long timeout) throws IOException {
        clean();
        try {
            return m_as.select(timeout);

        }
        finally {
            clean();
        }
    }

    @Override
    public int select() throws IOException {
        clean();
        try {
            return m_as.select();
        }
        finally {
            clean();
        }
    }

    private void clean() {
        Set<SelectionKey> keys = null;
        synchronized (cancelledKeys()) {
            keys = new HashSet<SelectionKey>(cancelledKeys());
            cancelledKeys().clear();
        }
        if (keys.isEmpty()) {
            return;
        }
        synchronized (m_keys) {
            for (SelectionKey key : keys) {
                deregister((AbstractSelectionKey) key);
                try {
                    ((SelectionKeyWrapper) key).m_result.cancel();
                    Method m = Util.getMethod(AbstractSelector.class,
                        "deregister", new Class[] { AbstractSelectionKey.class });
                    m.invoke(m_as, new Object[] { ((SelectionKeyWrapper) key).m_result });
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            for (SelectionKey key : keys) {
                m_keys.remove(((SelectionKeyWrapper) key).m_result);
            }
        }
    }

    private Set<SelectionKey> get(Set<SelectionKey> tmp) {
        if (tmp.isEmpty()) {
            return tmp;
        }
        Set<SelectionKey> resultS = new HashSet<SelectionKey>();
        synchronized (m_keys) {
            for (SelectionKey result : tmp) {
                SelectionKey key = m_keys.get(result);
                if (key != null) {
                    resultS.add(key);
                }
            }
        }
        return resultS;
    }

    @Override
    public Set<SelectionKey> keys() {
        clean();
        return get(m_as.keys());
    }

    private SelectionKey implReg(AbstractSelectableChannel ch, int ops,
            Object att) throws Exception {
        if (ch instanceof ServerSocketChannelWrapper) {
            return ((ServerSocketChannelWrapper) ch).m_ssc.register(m_as, ops, att);
        }
        if (ch instanceof SocketChannelWrapper) {

            return ((SocketChannelWrapper) ch).m_sc.register(m_as, ops, att);
        }
        if (ch instanceof DatagramChannelWrapper) {

            return ((DatagramChannelWrapper) ch).m_dc.register(m_as, ops, att);
        }
        Method m = Util.getMethod(m_as.getClass(), "register", new Class[] {
                AbstractSelectableChannel.class, Integer.TYPE, Object.class });
        m.setAccessible(true);
        return (SelectionKey) m.invoke(m_as, new Object[] { ch, ops, att });

    }

    @Override
    protected SelectionKey register(final AbstractSelectableChannel ch,
            int ops, Object att) {
        try {
            clean();
            final SelectionKey result = implReg(ch, ops, att);

            synchronized (m_keys) {
                SelectionKey key = m_keys.get(result);
                
                if (key == null) {
                    key = new SelectionKeyWrapper(this, result, ch);
                    m_keys.put(result, key);
                }
                return key;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new IllegalStateException();
        }
    }

    @Override
    protected void implCloseSelector() throws IOException {
        synchronized (m_keys) {
            m_keys.clear();
            /*
             * Method m = Util.getMethod(AbstractSelector.class,
             * "implCloseSelector", null);
             * try {
             * m.invoke(m_as, (Object[]) null);
             * }
             * catch (InvocationTargetException e) {
             * Throwable cause = e.getCause();
             * if (cause instanceof IOException) {
             * throw (IOException) cause;
             * }
             * else {
             * throw (RuntimeException) cause;
             * }
             * }
             * catch (Exception e) {
             * throw new RuntimeException(e);
             * }
             */
        }

        m_as.close();
    }
}