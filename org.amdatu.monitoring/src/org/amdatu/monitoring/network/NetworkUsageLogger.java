/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.nio.channels.DatagramChannel;
import java.nio.channels.Pipe;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelector;
import java.nio.channels.spi.SelectorProvider;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicLong;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;

/**
 * A network monitor. See the {@link Constants} class for the kind of values it measures.
 * <p>
 * It registers a {@link Socket} and a {@link ServerSocket} {@link SocketImplFactory}
 * as well as a {@link SelectorProvider} in order to keep track of connections and traffic
 * inside the JVM. This might not work on all JVMs (it does on Sun/Oracle) because we have 
 * to resort to reflection a lot (this is not public API) - ask Karl for more details.
 * </p>
 */
public class NetworkUsageLogger implements Probe {

    final class SocketImplFactoryWrapper implements SocketImplFactory {
        volatile SocketImplFactory m_orig = null;
        volatile Constructor<? extends SocketImpl> m_constructor = null;

        private final boolean m_server;

        public SocketImplFactoryWrapper() {
            this(false /* server */);
        }

        public SocketImplFactoryWrapper(boolean server) {
            this.m_server = server;
        }

        public SocketImpl createSocketImpl() {
            if (!m_server) {
                m_clientCounter.incrementAndGet();
            }
            if (m_orig != null) {
                return m_orig.createSocketImpl();
            }
            try {
                return m_constructor.newInstance();
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    final SelectorProvider m_newProvider = new SelectorProvider() {
        @Override
        public DatagramChannel openDatagramChannel() throws IOException {
            m_clientCounter.incrementAndGet();
            return new DatagramChannelWrapper(m_inputCounter, m_outputCounter, this, m_provider.openDatagramChannel());
        }

        @Override
        public Pipe openPipe() throws IOException {
            return m_provider.openPipe();
        }

        @Override
        public AbstractSelector openSelector() throws IOException {
            final AbstractSelector as = m_provider.openSelector();
            return as == null ? null : new SelectorWrapper(this, as);
        }

        @Override
        public ServerSocketChannel openServerSocketChannel() throws IOException {
            final ServerSocketChannel ssc = m_provider.openServerSocketChannel();
            return new ServerSocketChannelWrapper(m_inputCounter, m_outputCounter, m_serverCounter, this, ssc);
        }

        @Override
        public SocketChannel openSocketChannel() throws IOException {
            return new SocketChannelWrapper(m_inputCounter, m_outputCounter, this, m_provider.openSocketChannel());
        }
    };

    final StreamHandler m_handler = new StreamHandler() {
        public InputStream handleInputStream(final InputStream input) {
            return new InputStream() {
                public void close() throws IOException {
                    input.close();
                }

                public int read() throws IOException {
                    m_inputCounter.incrementAndGet();
                    return input.read();
                };

                public int read(byte[] b, int off, int len) throws IOException {
                    int v = input.read(b, off, len);
                    m_inputCounter.addAndGet(v);
                    return v;
                };
            };
        }

        public OutputStream handleOutputStream(final OutputStream output) {
            return new OutputStream() {
                public void close() throws IOException {
                    output.close();
                }

                public void write(byte[] b, int off, int len) throws IOException {
                    output.write(b, off, len);
                    m_outputCounter.addAndGet(len - off);
                };

                public void write(int b) throws IOException {
                    output.write(b);
                    m_outputCounter.incrementAndGet();
                }
            };
        }
    };

    final AtomicLong m_inputCounter = new AtomicLong(0);
    final AtomicLong m_outputCounter = new AtomicLong(0);
    final AtomicLong m_clientCounter = new AtomicLong(0);
    final AtomicLong m_serverCounter = new AtomicLong(0);

    private final SocketImplFactoryWrapper m_socketWrapper = new SocketImplFactoryWrapper();
    private final SocketImplFactoryWrapper m_serverSocketWrapper = new SocketImplFactoryWrapper(true);

    private volatile SelectorProvider m_provider = null;

    /**
     * @see MonitoringEvent
     */
    public Dictionary<String, Object> call() {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.KEY_PROBE_TYPE, Constants.NET_MONITOR_TYPE);

        long in = m_inputCounter.get();
        long out = m_outputCounter.get();
        long clientCount = m_clientCounter.get();
        long serverCount = m_serverCounter.get();
        
        props.put(Constants.NET_TRAFFIC_IN, in);
        props.put(Constants.NET_TRAFFIC_OUT, out);
        props.put(Constants.NET_CONNECTIONS_CLIENT, (clientCount - serverCount));
        props.put(Constants.NET_CONNECTIONS_SERVER, serverCount);
        
        return props;
    }

    synchronized void start() throws Exception {
        Socket s = new Socket();

        SocketImplFactory factory = new NetworkMonitoringSocketImplFactory(m_clientCounter, m_socketWrapper, m_handler);
        try {
            Socket.setSocketImplFactory(factory);
            m_socketWrapper.m_constructor = Util.getConstructor(s);
        }
        catch (SocketException ex) {
            m_socketWrapper.m_orig = Util.swapFactory(factory, Socket.class);
        }
        catch (Exception ex) {
            clean(m_socketWrapper, Socket.class);
            throw ex;
        }

        factory = new NetworkMonitoringSocketImplFactory(m_serverCounter, m_serverSocketWrapper, m_handler);

        try {
            ServerSocket.setSocketFactory(factory);
            if (m_socketWrapper.m_constructor == null) {
                m_serverSocketWrapper.m_constructor = Util.getConstructor(s);
            }
            else {
                m_serverSocketWrapper.m_constructor = m_socketWrapper.m_constructor;
            }
        }
        catch (SocketException ex) {
            try {
                m_serverSocketWrapper.m_orig = Util.swapFactory(factory,
                    ServerSocket.class);
            }
            catch (Exception e) {
                clean(m_socketWrapper, Socket.class);
                throw e;
            }
        }
        catch (Exception ex) {
            try {
                clean(m_socketWrapper, Socket.class);
            }
            finally {
                clean(m_serverSocketWrapper, ServerSocket.class);
            }
            throw ex;
        }
        try {
            m_provider = SelectorProvider.provider();
            Util.swapProvider(m_provider, m_newProvider);
        }
        catch (Exception ex) {
            try {
                clean(m_socketWrapper, Socket.class);
            }
            finally {
                clean(m_serverSocketWrapper, ServerSocket.class);
            }
            throw ex;
        }
    }

    synchronized void stop() throws Exception {
        try {
            Util.swapProvider(m_newProvider, m_provider);
        }
        finally {
            try {
                clean(m_socketWrapper, Socket.class);
            }
            finally {
                clean(m_serverSocketWrapper, ServerSocket.class);
            }
        }
    }

    private void clean(SocketImplFactoryWrapper wrapper, Class<?> clazz) throws IllegalAccessException, IOException {
        Util.swapFactory(wrapper.m_orig, clazz);
        wrapper.m_constructor = null;
        wrapper.m_orig = null;
    }
}
