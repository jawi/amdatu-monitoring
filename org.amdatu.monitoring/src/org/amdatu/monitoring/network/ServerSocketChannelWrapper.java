/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.atomic.AtomicLong;

final class ServerSocketChannelWrapper extends ServerSocketChannel {
    final ServerSocketChannel m_ssc;
    private final SelectorProvider m_provider;
    private final AtomicLong m_inputCounter;
    private final AtomicLong m_outputCounter;
    private final AtomicLong m_serverCounter;

    ServerSocketChannelWrapper(AtomicLong inputCounter, AtomicLong outputCounter, AtomicLong serverCounter, SelectorProvider provider,
            ServerSocketChannel ssc) {
        super(provider);
        m_inputCounter = inputCounter;
        m_outputCounter = outputCounter;
        m_serverCounter = serverCounter;
        m_provider = provider;
        m_ssc = ssc;
    }

    @Override
    protected void implConfigureBlocking(boolean block) throws IOException {
        m_ssc.configureBlocking(block);
    }

    @Override
    protected void implCloseSelectableChannel() throws IOException {
        m_ssc.close();
      /*  Method m = Util.getMethod(ServerSocketChannel.class,
                "implCloseSelectableChannel", null);
        try {
            m.invoke(m_ssc, (Object[]) null);
        }
        catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            else {
                throw (RuntimeException) cause;
            }
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }*/
    }

    @Override
    public ServerSocket socket() {
        return m_ssc.socket();
    }

    @Override
    public SocketChannel accept() throws IOException {
        SocketChannel channel = m_ssc.accept();
        if (channel != null) {
            m_serverCounter.incrementAndGet();
            return new SocketChannelWrapper(m_inputCounter, m_outputCounter, m_provider, channel);
        }
        return null;
    }
    @Override
    public boolean equals(Object obj) {
        return m_ssc.equals((obj instanceof ServerSocketChannelWrapper) ? ((ServerSocketChannelWrapper) obj).m_ssc : obj);
    }
    @Override
    public int hashCode() {
        return m_ssc.hashCode();
    }
}