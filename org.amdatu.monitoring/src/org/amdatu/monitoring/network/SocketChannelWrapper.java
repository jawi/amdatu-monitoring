/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.atomic.AtomicLong;

final class SocketChannelWrapper extends SocketChannel {
    final SocketChannel m_sc;
    private final AtomicLong m_inputCounter;
    private final AtomicLong m_outputCounter;

    SocketChannelWrapper(AtomicLong inputCounter, AtomicLong outputCounter, SelectorProvider provider, SocketChannel sc) {
        super(provider);
        m_inputCounter = inputCounter;
        m_outputCounter = outputCounter;
        m_sc = sc;
    }

    @Override
    protected void implConfigureBlocking(boolean block) throws IOException {
        m_sc.configureBlocking(block);
    }

    @Override
    protected void implCloseSelectableChannel() throws IOException {
        m_sc.close();
     /*   Method m = Util.getMethod(SocketChannel.class,
                "implCloseSelectableChannel", null);
        try {
            m.invoke(m_sc, (Object[]) null);
        }
        catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            else {
                throw (RuntimeException) cause;
            }
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }*/
    }

    @Override
    public long write(ByteBuffer[] srcs, int offset, int length)
            throws IOException {
        long result = m_sc.write(srcs, offset, length);
        m_outputCounter.addAndGet(result);
        return result;
    }

    @Override
    public int write(ByteBuffer src) throws IOException {
        int result = m_sc.write(src);
        m_outputCounter.addAndGet(result);
        return result;
    }

    @Override
    public Socket socket() {
        
        return m_sc.socket();
    }

    @Override
    public long read(ByteBuffer[] dsts, int offset, int length)
            throws IOException {
        long result = m_sc.read(dsts, offset, length);
        m_inputCounter.addAndGet(result);
        return result;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        int result = m_sc.read(dst);
        m_inputCounter.addAndGet(result);
        return result;
    }

    @Override
    public boolean isConnectionPending() {
        return m_sc.isConnectionPending();
    }

    @Override
    public boolean isConnected() {
        return m_sc.isConnected();
    }

    @Override
    public boolean finishConnect() throws IOException {
        return m_sc.finishConnect();
    }

    @Override
    public boolean connect(SocketAddress remote) throws IOException {
        return m_sc.connect(remote);
    }
    @Override
    public boolean equals(Object obj) {
        return m_sc.equals((obj instanceof SocketChannelWrapper) ? ((SocketChannelWrapper) obj).m_sc : obj);
    }
    @Override
    public int hashCode() {
        return m_sc.hashCode();
    }
}