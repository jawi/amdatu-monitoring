/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.DatagramSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.atomic.AtomicLong;

final class DatagramChannelWrapper extends DatagramChannel {
    final DatagramChannel m_dc;
    private final AtomicLong m_inputCounter;
    private final AtomicLong m_outputCounter;

    protected DatagramChannelWrapper(AtomicLong inputCounter, AtomicLong outputCounter, SelectorProvider provider,
        DatagramChannel dc) {
        super(provider);
        m_inputCounter = inputCounter;
        m_outputCounter = outputCounter;
        m_dc = dc;
    }

    @Override
    public DatagramSocket socket() {
        return m_dc.socket();
    }

    @Override
    public boolean isConnected() {
        return m_dc.isConnected();
    }

    @Override
    public DatagramChannel connect(SocketAddress remote) throws IOException {
        return m_dc.connect(remote);
    }

    @Override
    public DatagramChannel disconnect() throws IOException {
        return m_dc.disconnect();
    }

    @Override
    public SocketAddress receive(ByteBuffer dst) throws IOException {
        long start = dst.position();
        SocketAddress result = m_dc.receive(dst);
        m_inputCounter.addAndGet(dst.position() - start);
        return result;
    }

    @Override
    public int send(ByteBuffer src, SocketAddress target) throws IOException {
        int result = m_dc.send(src, target);
        m_outputCounter.addAndGet(result);
        return result;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        int result = m_dc.read(dst);
        m_inputCounter.addAndGet(result);
        return result;
    }

    @Override
    public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
        long result = m_dc.read(dsts, offset, length);
        m_inputCounter.addAndGet(result);
        return result;
    }

    @Override
    public int write(ByteBuffer src) throws IOException {
        int result = m_dc.write(src);
        m_outputCounter.addAndGet(result);
        return result;
    }

    @Override
    public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
        long result = m_dc.write(srcs, offset, length);
        m_outputCounter.addAndGet(result);
        return result;
    }

    @Override
    protected void implCloseSelectableChannel() throws IOException {
        Method m = Util.getMethod(DatagramChannel.class, "implCloseSelectableChannel", null);
        try {
            m.invoke(m_dc, (Object[]) null);
        }
        catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            else {
                throw (RuntimeException) cause;
            }
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void implConfigureBlocking(boolean block) throws IOException {
        m_dc.configureBlocking(block);
    }
}