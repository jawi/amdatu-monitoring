/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.nio.channels.spi.SelectorProvider;

public class Util {

    public static Constructor<? extends SocketImpl> getConstructor(Socket s) throws IllegalAccessException, NoSuchMethodException {
        Class<? extends SocketImpl> socketImplClass = null;
        for (Field f : s.getClass().getDeclaredFields()) {
            if (SocketImpl.class.isAssignableFrom(f.getType())) {
                f.setAccessible(true);
                SocketImpl impl = (SocketImpl) f.get(s);
                if (impl != null) {
                    if (socketImplClass != null) {
                        if (!socketImplClass.equals(impl.getClass())) {
                            throw new IllegalStateException("Unable to decide which default socketimpl to use");
                        }
                    }
                    else {
                        socketImplClass = impl.getClass();
                    }
                }
            }
        }
        if (socketImplClass == null) {
            throw new IllegalStateException("Unable to determine default socketimpl");
        }
        final Constructor<? extends SocketImpl> c = socketImplClass.getDeclaredConstructor((Class[]) null);
        if (c != null) {
            c.setAccessible(true);
        }
        else {
            throw new IllegalStateException("Unable to determine default socketImpl with default constructor");
        }
        return c;
    }

    public static Method getMethod(Class<?> current, String name, Class<?>[] parameterTypes) {
        while (current != null) {
            try {
                Method result = current.getDeclaredMethod(name, parameterTypes);
                result.setAccessible(true);
                return result;
            }
            catch (Exception ex) {
                // Ignore...
            }
            current = current.getSuperclass();
        }
        throw new RuntimeException();
    }

    public static SocketImplFactory swapFactory(SocketImplFactory factory, Class<?> org) throws IllegalAccessException, IOException {
        Class<?> current = org;
        while (current != null) {
            for (Field field : current.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers()) && SocketImplFactory.class.isAssignableFrom(field.getType())) {
                    field.setAccessible(true);
                    SocketImplFactory fac = (SocketImplFactory) field.get(null);
                    if (fac != null) {
                        field.set(null, null);
                        if (org.equals(Socket.class)) {
                            Socket.setSocketImplFactory(factory);
                        }
                        else {
                            ServerSocket.setSocketFactory(factory);
                        }
                        return fac;
                    }
                }
            }
            current = current.getSuperclass();
        }
        throw new IllegalStateException("Unable to replace factory");
    }

    public static void swapProvider(SelectorProvider provider, SelectorProvider newProvider) throws Exception {
        Class<?> current = SelectorProvider.class;
        while (current != null) {
            for (Field field : current.getDeclaredFields()) {
                if (Modifier.isStatic(field.getModifiers()) && SelectorProvider.class.isAssignableFrom(field.getType())) {
                    field.setAccessible(true);
                    if (provider == field.get(null)) {
                        field.set(null, newProvider);
                        return;
                    }
                }
            }
            current = current.getSuperclass();
        }
        throw new IllegalStateException();
    }
}
