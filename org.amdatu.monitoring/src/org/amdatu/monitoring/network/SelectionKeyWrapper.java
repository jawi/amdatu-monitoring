/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.channels.spi.AbstractSelectionKey;

final class SelectionKeyWrapper extends AbstractSelectionKey {
    private final Selector m_ss;
    final SelectionKey m_result;
    private final AbstractSelectableChannel m_ch;

    SelectionKeyWrapper(Selector ss, SelectionKey result,
            AbstractSelectableChannel ch) {
        m_ss = ss;
        m_result = result;
        m_ch = ch;
    }

    @Override
    public Selector selector() {
        return m_ss;
    }

    @Override
    public int readyOps() {
        return m_result.readyOps();
    }

    @Override
    public SelectionKey interestOps(int ops) {
        m_result.interestOps(ops);
        return this;
    }

    @Override
    public int interestOps() {
        return m_result.interestOps();
    }

    @Override
    public SelectableChannel channel() {
        return m_ch;
    }
    
    @Override
    public boolean equals(Object obj) {
        return m_result.equals((obj instanceof SelectionKeyWrapper) ? ((SelectionKeyWrapper) obj).m_result : obj);
    }
    @Override
    public int hashCode() {
        return m_result.hashCode();
    }
}