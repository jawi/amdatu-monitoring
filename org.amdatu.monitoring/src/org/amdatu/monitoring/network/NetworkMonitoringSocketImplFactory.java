/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.net.SocketImpl;
import java.net.SocketImplFactory;
import java.util.concurrent.atomic.AtomicLong;

final class NetworkMonitoringSocketImplFactory implements SocketImplFactory {
    final SocketImplFactory m_factory;
    final StreamHandler m_handler;
    final AtomicLong m_connections;

    NetworkMonitoringSocketImplFactory(AtomicLong connections, SocketImplFactory factory, StreamHandler handler) {
        m_connections = connections;
        m_factory = factory;
        m_handler = handler;
    }

    public SocketImpl createSocketImpl() {
        try {
            return new NetworkMonitoringSocketImpl(m_connections, m_factory.createSocketImpl(), m_handler);
        }
        catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }
}