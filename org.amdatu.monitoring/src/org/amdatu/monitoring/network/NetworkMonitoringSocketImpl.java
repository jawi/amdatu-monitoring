/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.util.concurrent.atomic.AtomicLong;

final class NetworkMonitoringSocketImpl extends SocketImpl {

    final SocketImpl m_delegate;
    final StreamHandler m_handler;
    final AtomicLong m_connections;

    public NetworkMonitoringSocketImpl() {
        m_delegate = null;
        m_handler = null;
        m_connections = null;
    }

    NetworkMonitoringSocketImpl(AtomicLong connections, SocketImpl socketImpl, StreamHandler handler) {
        m_delegate = socketImpl;
        m_handler = handler;
        m_connections = connections;
    }

    private Method getMethod(Class<?> current, String name, Class<?>[] parameterTypes) {
        while (current != null) {
            try {
                Method result = current.getDeclaredMethod(name, parameterTypes);
                result.setAccessible(true);
                return result;
            }
            catch (Exception ex) {}
            current = current.getSuperclass();
        }
        throw new RuntimeException();
    }

    private Object call(String method, Class<?>[] argsT, Object[] args)
            throws IOException {
   //     System.out.println(this + "." + method + ((args == null) ? "" : Arrays.asList(args)));
        try {
            Method m = getMethod(m_delegate.getClass(), method, argsT);
            return m.invoke(m_delegate, args);
        }
        catch (InvocationTargetException ex) {
            throw (IOException) ex.getCause();
        }
        catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    public void setOption(int optID, Object value) throws SocketException {
        try {
            call("setOption", new Class[] { Integer.TYPE, Object.class },
                    new Object[] { optID, value });
        }
        catch (IOException e) {
            throw (SocketException) e;
        }
    }

    public Object getOption(int optID) throws SocketException {
        try {
            return call("getOption", new Class[] { Integer.TYPE },
                    new Object[] { optID });
        }
        catch (IOException e) {
            throw (SocketException) e;
        }
    }

    @Override
    protected void sendUrgentData(int data) throws IOException {
        call("sendUrgentData", new Class[] { Integer.TYPE },
                new Object[] { data });
    }

    @Override
    protected void listen(int backlog) throws IOException {
        call("listen", new Class[] { Integer.TYPE }, new Object[] { backlog });
    }

    @Override
    protected OutputStream getOutputStream() throws IOException {
        return m_handler.handleOutputStream((OutputStream) call(
                "getOutputStream", null, null));
    }

    @Override
    protected InputStream getInputStream() throws IOException {
        return m_handler.handleInputStream((InputStream) call("getInputStream",
                null, null));
    }

    @Override
    protected void create(boolean stream) throws IOException {
        call("create", new Class[] { Boolean.TYPE }, new Object[] { stream });
        try {
            copy("address", m_delegate, this);
            copy("port", m_delegate, this);
            copy("fd", m_delegate, this);
            copy("localport", m_delegate, this);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    protected void connect(SocketAddress address, int timeout)
            throws IOException {
        call("connect", new Class[] { SocketAddress.class, Integer.TYPE },
                new Object[] { address, timeout });
        try {
            copy("address", m_delegate, this);
            copy("port", m_delegate, this);
            copy("fd", m_delegate, this);
            copy("localport", m_delegate, this);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    protected void connect(InetAddress address, int port) throws IOException {
        call("connect", new Class[] { InetAddress.class, Integer.TYPE },
                new Object[] { address, port });
        try {
            copy("address", m_delegate, this);
            copy("port", m_delegate, this);
            copy("fd", m_delegate, this);
            copy("localport", m_delegate, this);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    protected void connect(String host, int port) throws IOException {
        call("connect", new Class[] { String.class, Integer.TYPE },
                new Object[] { host, port });
        try {
            copy("address", m_delegate, this);
            copy("port", m_delegate, this);
            copy("fd", m_delegate, this);
            copy("localport", m_delegate, this);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    protected void close() throws IOException {
        call("close", null, null);
    }
    
    @Override
    protected void shutdownInput() throws IOException {
        call("shutdownInput", null, null);
    };
    
    @Override
    protected void shutdownOutput() throws IOException {
        call("shutdownOutput", null, null);
    };
    
    @Override
    protected boolean supportsUrgentData() {
        try {
            return (Boolean) call("supportsUrgentData", null, null);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    };

    @Override
    protected void bind(InetAddress host, int port) throws IOException {
        call("bind", new Class[] { InetAddress.class, Integer.TYPE },
                new Object[] { host, port });
        
        try {
            copy("address", m_delegate, this);
            copy("port", m_delegate, this);
            copy("fd", m_delegate, this);
            copy("localport", m_delegate, this);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }

    @Override
    protected int available() throws IOException {
        return (Integer) call("available", null, null);
    }

    private Field getField(Class<?> start, String name)
            throws NoSuchFieldException {
        while (start != null) {
            try {
                Field result = start.getDeclaredField(name);
                result.setAccessible(true);
                return result;
            }
            catch (Exception ex) {}
            start = start.getSuperclass();
        }
        throw new NoSuchFieldException(name);
    }

    private void copy(String fieldName, Object org, Object target)
            throws IllegalArgumentException, IllegalAccessException,
            NoSuchFieldException {
        Field targetF = getField(target.getClass(), fieldName);
        Field orgF = getField(org.getClass(), fieldName);
        targetF.set(target, orgF.get(org));
    }

    @Override
    protected void accept(SocketImpl s) throws IOException {
        SocketImpl sDelegate = ((NetworkMonitoringSocketImpl) s).m_delegate;
        try {
            copy("address", s, sDelegate);
            copy("port", s, sDelegate);
            copy("fd", s, sDelegate);

            copy("localport", s, sDelegate);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
        m_connections.incrementAndGet();
        call("accept", new Class[] { SocketImpl.class },
                new Object[] { sDelegate });
        try {
            copy("address", sDelegate, s);
            copy("port", sDelegate, s);
            copy("fd", sDelegate, s);
            copy("localport", sDelegate, s);
        }
        catch (Exception ex) {
            throw new IOException(ex);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        System.out.println("equals");
        return m_delegate.equals((obj instanceof NetworkMonitoringSocketImpl) ? ((NetworkMonitoringSocketImpl) obj).m_delegate : obj);
    }
    
    @Override
    public int hashCode() {
        return m_delegate.hashCode();
    }
}