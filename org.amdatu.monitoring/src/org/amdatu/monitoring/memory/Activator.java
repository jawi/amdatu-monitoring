/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.memory;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Publish the memory usage service.
 */
public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.KEY_PROBE_NAME, Constants.MEM_MONITOR_NAME);
        props.put(Constants.KEY_PROBE_TYPE, Constants.MEM_MONITOR_TYPE);

        context.registerService(Probe.class.getName(), new MemoryUsageLogger(), props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        // Nop...
    }
}
