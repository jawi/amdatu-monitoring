/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.memory;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;

/**
 * Provides a memory usage monitor.
 */
public class MemoryUsageLogger implements Probe {

    @Override
    public Dictionary<String, Object> call() throws Exception {
        Dictionary<String, Object> result = new Hashtable<String, Object>();
        result.put(Constants.KEY_PROBE_TYPE, Constants.MEM_MONITOR_TYPE);
        
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        
        MemoryUsage heapMem = memoryMXBean.getHeapMemoryUsage();
        result.put(Constants.MEM_HEAP_USED, heapMem.getUsed());
        result.put(Constants.MEM_HEAP_COMMITTED, heapMem.getCommitted());
        result.put(Constants.MEM_HEAP_MAX, heapMem.getMax());
        
        MemoryUsage nonHeapMem = memoryMXBean.getNonHeapMemoryUsage();
        result.put(Constants.MEM_NON_HEAP_USED, nonHeapMem.getUsed());
        result.put(Constants.MEM_NON_HEAP_COMMITTED, nonHeapMem.getCommitted());
        result.put(Constants.MEM_NON_HEAP_MAX, nonHeapMem.getMax());

        return result;
    }

}
