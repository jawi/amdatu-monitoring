/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring;

import java.util.Dictionary;

/**
 * Denotes a probe for sampling any kind of (system) information.
 * <p>
 * A probe should be registered as normal OSGi-service, with two <em>mandatory</em> service properties:
 * </p>
 * <ol>
 * <li>{@link Constants#KEY_PROBE_NAME}, with a probe name as (String) value. And,</li>
 * <li>{@link Constants#KEY_PROBE_TYPE}, with a event type as (String) value.</li>
 * </ol>
 */
public interface Probe {

    /**
     * Invokes this probe, which should take a snapshot of the monitored information and
     * return it as a {@link Dictionary}.
     * <p>
     * Implementations should always put the value of {@link Constants#KEY_PROBE_TYPE} 
     * corresponding to the type of this monitor in the resulting dictionary as to
     * identify it properly.
     * </p>
     * <p>
     * The values of the returned dictionary should be as much as possible limited to
     * primitive values, as {@link Integer}, {@link Long}, {@link String} and so on.
     * </p>
     * 
     * @return a dictionary with all monitoring information, never <code>null</code>.
     * @throws Exception in case this monitor failed due to an unknown/unforeseen reason.
     */
    Dictionary<String, Object> call() throws Exception;
}
