/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring;

/**
 * Provides constant values used by the monitor implementations.
 */
public interface Constants {
    /**
     * Provides a symbolic name (as String value) for the probe.
     */
    String KEY_PROBE_NAME = "name";

    /**
     * Denotes the type of events the implementing monitor emits. Its value is an Integer value.
     */
    String KEY_PROBE_TYPE = "type";

    /**
     * The name used for the CPU monitor.
     */
    String CPU_MONITOR_NAME = "cpu";

    /**
     * Denotes a CPU load measurement which will have a {@link #CPU_COUNT}, {@link #CPU_ACTIVE_THREADS}, {@link #CPU_TOTAL_TIME}, {@link #CPU_AVG_LOAD}, and a {@link #CPU_PEAK_LOAD} entry.
     */
    Integer CPU_MONITOR_TYPE = 1;

    /**
     * The number of CPUs on the target (integer value).
     */
    String CPU_COUNT = "cpu";
    
    /**
     * The number of active threads (integer value).
     */
    String CPU_ACTIVE_THREADS = "threads";

    /**
     * The total amount of CPU time spend (long value).
     */
    String CPU_TOTAL_TIME = "total";

    /**
     * The average CPU load caused by the target as a percentage (integer value).
     */
    String CPU_AVG_LOAD = "avg";

    /**
     * The current CPU load caused by the target as a percentage (integer value).
     */
    String CPU_PEAK_LOAD = "peak";

    /**
     * The name used for the network usage monitor.
     */
    String NET_MONITOR_NAME = "network";
    
    /**
     * Denotes a network load measurement which will have a {@link #NET_CONNECTIONS_SERVER}, {@link #NET_CONNECTIONS_CLIENT}, {@link #NET_TRAFFIC_IN}, and a {@link #NET_TRAFFIC_OUT} entry.
     */
    Integer NET_MONITOR_TYPE = 2;

    /**
     * The number of connections made to this target as a server (long value).
     */
    String NET_CONNECTIONS_SERVER = "connections-server";

    /**
     * The number of connections made from this target as a client (long value).
     */
    String NET_CONNECTIONS_CLIENT = "connections-client";

    /**
     * The amount of bytes send out from this target (long value).
     */
    String NET_TRAFFIC_OUT = "traffic-out";

    /**
     * The amount of bytes received by this target (long value).
     */
    String NET_TRAFFIC_IN = "traffic-in";

    /**
     * The name used for the disk usage monitor.
     */
    String DISK_MONITOR_NAME = "disk";
    
    /**
     * Denotes a disc measurement which will have entries with a {@link #DISK_PERCENTAGE_SUFFIX}, {@link #DISK_FREE_SUFFIX}, and a {@link #DISK_TOTAL_SUFFIX} as suffix.
     */
    Integer DISK_MONITOR_TYPE = 4;

    /**
     * The part of the key up to the suffix is the name of the partition and 
     * the value is the used space as a percentage (integer value).
     */
    String DISK_PERCENTAGE_SUFFIX = "-percentage";

    /**
     * The part of the key up to the suffix is the name of the partition and 
     * the value is the free space in bytes (long value).
     */
    String DISK_FREE_SUFFIX = "-free";

    /**
     * The part of the key up to the suffix is the name of the partition and 
     * the value is the total available space in bytes (long value).
     */
    String DISK_TOTAL_SUFFIX = "-total";

    /**
     * The name used for the memory usage monitor.
     */
    String MEM_MONITOR_NAME = "memory";
    
    /**
     * Denotes a memory measurement that will have  {@link #MEM_HEAP_USED}, {@link #MEM_HEAP_COMMITTED}, {@link #MEM_HEAP_MAX}, {@link #MEM_NON_HEAP_USED}, {@link #MEM_NON_HEAP_COMMITTED} and {@link #MEM_NON_HEAP_MAX} as entries.
     */
    Integer MEM_MONITOR_TYPE = 8;

    /**
     * The amount of used heap memory, in bytes (long value).
     */
    String MEM_HEAP_USED = "used";
    
    /**
     * The amount of committed heap memory, in bytes (long value).<br/>
     * This is the amount of memory that is guaranteed for the JVM to use.
     */
    String MEM_HEAP_COMMITTED = "committed";

    /**
     * The maximum amount of heap memory, in bytes (long value), <tt>-1</tt> 
     * if undefined.
     */
    String MEM_HEAP_MAX = "max";

    /**
     * The amount of used non-heap memory, in bytes (long value).
     */
    String MEM_NON_HEAP_USED = "used";
    
    /**
     * The amount of committed non-heap memory, in bytes (long value).<br/>
     * This is the amount of memory that is guaranteed for the JVM to use.
     */
    String MEM_NON_HEAP_COMMITTED = "committed";

    /**
     * The maximum amount of non-heap memory, in bytes (long value), 
     * <tt>-1</tt> if undefined.
     */
    String MEM_NON_HEAP_MAX = "max";

}
