/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.cpu;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;

/**
 * A CPU monitor. See the {@link Constants} class for the kind of values it measures.
 * <p>
 * It uses a JVM implementation specific MXBean to get the CPU values which might not
 * be available on all JVMs (it is on Sun/Oracle's).
 * </p>
 */
public class CpuUsageLogger implements Probe {
    private final int m_cpuCount = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();

    private volatile long m_startTime = -1;
    private volatile long m_startSystem = -1;
    private volatile long m_lastSystem = -1;
    private volatile long m_lastTime = -1;

    /**
     * @see Constants
     */
    public Dictionary<String, Object> call() {
        long currentTime = System.nanoTime();
        long currentSystem = getTotalCpu();
        long diffTime = currentTime - m_lastTime;
        long diffSystem = currentSystem - m_lastSystem;

        int activeThreadCount = ManagementFactory.getThreadMXBean().getAllThreadIds().length;

        m_lastTime = currentTime;
        m_lastSystem = currentSystem;

        int peak = (int) (Math.min(m_cpuCount, ((double) diffSystem) / diffTime) * 100);
        int avg = (int) (Math.min(m_cpuCount, ((double) (currentSystem - m_startSystem)) / (currentTime - m_startTime)) * 100);

        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(Constants.KEY_PROBE_TYPE, Constants.CPU_MONITOR_TYPE);
        props.put(Constants.CPU_PEAK_LOAD, peak);
        props.put(Constants.CPU_AVG_LOAD, avg);
        props.put(Constants.CPU_TOTAL_TIME, currentSystem / 1e9);
        props.put(Constants.CPU_ACTIVE_THREADS, activeThreadCount);
        props.put(Constants.CPU_COUNT, m_cpuCount);

        return props;
    }

    final void start() {
        if (m_lastSystem == -1) {
            m_lastSystem = m_startSystem = getTotalCpu();
            if (m_lastSystem == -1) {
                throw new IllegalStateException("System doesn't support CPU monitoring");
            }
            m_lastTime = m_startTime = System.nanoTime();
        }
    }

    final void stop() {
        if (m_lastSystem != -1) {
            m_lastSystem = -1;
            m_lastTime = -1;
        }
    }

    private long getTotalCpu() {
        Long value = null;
        try {
            OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
            Method m = os.getClass().getMethod("getProcessCpuTime");
            m.setAccessible(true);
            value = (Long) m.invoke(os);
        }
        catch (Exception ex) {
            // TODO: log this or something
        }
        return (value == null) ? -1L : value.longValue();
    }
}
