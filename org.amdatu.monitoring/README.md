# Amdatu Monitoring

The Amdatu monitoring project is a small framework for monitoring information of any kind
in a running JVM or system.


## Probes

The monitoring project defines the concept of a "probe", which is something that takes a
sample of information one is interested in. For example, the current CPU load, or the 
current memory usage.

A probe should register itself as plain OSGi service, using two **mandatory** service
properties:

 - Constants#KEY_PROBE_NAME, representing a short meaningful name for the probe, and;
 - Constants#KEY_PROBE_TYPE, representing an Integer value. This value is used to 
   distinguish between various probe samples (see below).

Each probe **must** implement the <tt>org.amdatu.monitoring.Probe</tt> interface, which
defines a single method: <tt>call()</tt>. This method returns a dictionary representing
the sampled values. The dictionary is a mapping between string keys and object values,
allowing any kind of information returned by a probe. It is advised to return values that 
properly implement a "Object#toString()" method (as almost all Java primitives and their 
corresponding wrappers do).

To distinguish between the samples of various probes, each returned dictionary should
contain the Constants#KEY_PROBE_TYPE along with the type value associated to the probe
(which is the same value used for the service registration properties).


### Example

To show how a probe works, we implement a simple date/time probe, which simply returns
the current date/time. You can use the probe API as your build/compile time dependency,
provided by <tt>org.amdatu.monitoring.api.jar</tt>. You also need a recent version of the
OSGi core bundle, for example, version 4.2 or later.

First, we implement the DateTimeProbe itself:

  public class DateTimeProbe implements Probe {
    public static final String NAME = "Date/time";
    public static final Integer TYPE = 64;
    
    public Dictionary<String, Object> call() throws Exception {
      Dictionary<String, Object> result = new Hashtable<String, Object>();
      result.put(Constants.KEY_PROBE_TYPE, TYPE);
      result.put("timestamp", Long.valueOf(System.currentTimeMillis()));
      return result;
    }
  }

The second thing we need to do is to register it in our BundleActivator:

  public class Activator implements BundleActivator {
    public void start(BundleContext context) throws Exception {
      Dictionary<String, Object> props = new Hashtable<String, Object>();
      props.put(Constants.KEY_PROBE_NAME, DateTimeProbe.NAME);
      props.put(Constants.KEY_PROBE_TYPE, DateTimeProbe.TYPE);
      
      context.registerService(Probe.class.getName(), new DateTimeProbe(), props);
    }
    
    public void stop(BundleContext context) {
      // Nop
    }
  }

Packaging the date/time probe is trivial when using a tool like Bnd, which looks like:

  Bundle-Version: 1.0.0
  Bundle-Activator: org.amdatu.monitoring.datetime.Activator  
  Private-Package: org.amdatu.monitoring.datetime

That's it. You can now deploy this in your OSGi environment and write a consumer that 
call all probes at regular intervals to take samples.


## License

This project is licensed under the Apache License, Version 2.0.
