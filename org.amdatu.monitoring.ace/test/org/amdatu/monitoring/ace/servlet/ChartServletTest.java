/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.servlet;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

import junit.framework.TestCase;

import org.amdatu.monitoring.ace.servlet.ChartServlet.Parameters;

/**
 * Test cases for {@link ChartServlet}.
 */
public class ChartServletTest extends TestCase {

    private ChartServlet m_servlet;
    private Parameters m_params;

    /**
     * Tests that creating a chart without any data works.
     */
    public void testCreateEmptyChartHtmlOk() throws IOException {
        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String, Object>>();

        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({});"));
    }

    /**
     * Tests that creating a chart with some data works.
     */
    public void testCreateSimpleChartHtmlOk() throws IOException {
        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3));
        data.put(2L, createSample("foo", 1));
        data.put(3L, createSample("foo", 2));
        
        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({cols: [{label:'Time', type:'date'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 3}]},{c:[{v: new Date(2)},{v: 1}]},{c:[{v: new Date(3)},{v: 2}]}]});"));
    }

    /**
     * Tests that creating a chart with some data works.
     */
    public void testCreateTwoVariableChartHtmlOk() throws IOException {
        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));
        
        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({cols: [{label:'Time', type:'date'},{label:'bar', type:'number'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 2},{v: 3}]},{c:[{v: new Date(2)},{v: 3},{v: 1}]},{c:[{v: new Date(3)},{v: 1},{v: 2}]}]});"));
    }

    /**
     * Tests that creating a chart of another type with some data works.
     */
    public void testCreateOtherTypeChartHtmlOk() throws IOException {
        m_params.m_chartType = "ColumnChart";
        
        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));
        
        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({cols: [{label:'Time', type:'date'},{label:'bar', type:'number'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 2},{v: 3}]},{c:[{v: new Date(2)},{v: 3},{v: 1}]},{c:[{v: new Date(3)},{v: 1},{v: 2}]}]});"));
        assertTrue(result.contains("google.visualization.ColumnChart"));
    }

    /**
     * Tests that creating a chart of another type with some data works.
     */
    public void testCreateBoundedChartHtmlOk() throws IOException {
        m_params.m_start = 2;
        m_params.m_end = 4;

        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));
        data.put(4L, createSample("foo", 3, "bar", 2));
        data.put(5L, createSample("foo", 1, "bar", 3));
        
        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({cols: [{label:'Time', type:'date'},{label:'bar', type:'number'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(2)},{v: 3},{v: 1}]},{c:[{v: new Date(3)},{v: 1},{v: 2}]},{c:[{v: new Date(4)},{v: 2},{v: 3}]}]});"));
    }

    /**
     * Tests that creating a chart with some data and additional options works.
     */
    public void testCreateChartHtmlWithAdditionalOptionsOk() throws IOException {
        m_params.m_options = new String[] { "'isStacked':true" };

        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));
        data.put(4L, createSample("foo", 3, "bar", 2));
        data.put(5L, createSample("foo", 1, "bar", 3));
        
        String result = m_servlet.createChartHtml(data, m_params);
        assertNotNull(result);

        assertTrue(result.contains("google.visualization.DataTable({cols: [{label:'Time', type:'date'},{label:'bar', type:'number'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 2},{v: 3}]},{c:[{v: new Date(2)},{v: 3},{v: 1}]},{c:[{v: new Date(3)},{v: 1},{v: 2}]},{c:[{v: new Date(4)},{v: 2},{v: 3}]},{c:[{v: new Date(5)},{v: 3},{v: 1}]}]});"));
    }

    /**
     * Tests that we can JSONify sample data which contains one variable.
     */
    public void testJsonifySingleVariableOk() {
        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3));
        data.put(2L, createSample("foo", 1));
        data.put(3L, createSample("foo", 2));
        
        StringBuilder sb = new StringBuilder();
        m_servlet.jsonifySampleData(sb, data, m_params);
        
        String json = sb.toString();
        assertNotNull(json);

        assertEquals("{cols: [{label:'Time', type:'date'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 3}]},{c:[{v: new Date(2)},{v: 1}]},{c:[{v: new Date(3)},{v: 2}]}]}", json);
    }

    /**
     * Tests that we can JSONify sample data which contains more than one variable.
     */
    public void testJsonifyTwoVariablesOk() {
        m_params.m_keys = new String[]{ "foo", "bar" };

        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));

        StringBuilder sb = new StringBuilder();
        m_servlet.jsonifySampleData(sb, data, m_params);
        
        String json = sb.toString();
        assertNotNull(json);

        assertEquals("{cols: [{label:'Time', type:'date'},{label:'foo', type:'number'},{label:'bar', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 3},{v: 2}]},{c:[{v: new Date(2)},{v: 1},{v: 3}]},{c:[{v: new Date(3)},{v: 2},{v: 1}]}]}", json);
    }

    /**
     * Tests that we can JSONify sample data which contains more than one variable with the order as supplied.
     */
    public void testJsonifyTwoVariablesOrderFixedOk() {
        m_params.m_keys = new String[]{ "bar", "foo" };

        Map<Long, Dictionary<String, Object>> data = new TreeMap<Long, Dictionary<String,Object>>();
        data.put(1L, createSample("foo", 3, "bar", 2));
        data.put(2L, createSample("foo", 1, "bar", 3));
        data.put(3L, createSample("foo", 2, "bar", 1));

        StringBuilder sb = new StringBuilder();
        m_servlet.jsonifySampleData(sb, data, m_params);
        
        String json = sb.toString();
        assertNotNull(json);

        assertEquals("{cols: [{label:'Time', type:'date'},{label:'bar', type:'number'},{label:'foo', type:'number'}], rows: [{c:[{v: new Date(1)},{v: 2},{v: 3}]},{c:[{v: new Date(2)},{v: 3},{v: 1}]},{c:[{v: new Date(3)},{v: 1},{v: 2}]}]}", json);
    }

    @Override
    protected void setUp() throws Exception {
        m_servlet = new ChartServlet();

        m_params = new Parameters();
        m_params.m_chartType = "AreaChart";
        m_params.m_width = 400;
        m_params.m_height = 300;
        m_params.m_targetId = "foo";
    }

    private Dictionary<String, Object> createSample(String key, Object value) {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(key, value);
        return props;
    }

    private Dictionary<String, Object> createSample(String key1, Object value1, String key2, Object value2) {
        Dictionary<String, Object> props = new Hashtable<String, Object>();
        props.put(key1, value1);
        props.put(key2, value2);
        return props;
    }
}
