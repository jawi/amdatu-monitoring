/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart.impl;

import java.net.URL;

import org.amdatu.monitoring.ace.chart.ChartBuilder;

import junit.framework.TestCase;

/**
 * Test cases for {@link ChartBuilderImpl}.
 */
public class ChartBuilderImplTest extends TestCase {

    /**
     * Tests that building a simple chart URL works correctly.
     */
    public void testBuildSimpleChartOk() throws Exception {
        ChartBuilder builder = new ChartBuilderImpl("http://host:1234/path/to/servlet")
            .setChartType("fooChart")
            .setTargetID("target")
            .setWidth(640)
            .setHeight(480);

        URL url = builder.build();
        assertNotNull(url);

        assertEquals(
            "http://host:1234/path/to/servlet?probeType=0&targetID=target&width=640&height=480&resolution=100&chartType=fooChart",
            url.toExternalForm());
    }

    /**
     * Tests that building a simple chart URL works correctly.
     */
    public void testBuildChartWithoutChartTypeOk() throws Exception {
        ChartBuilder builder = new ChartBuilderImpl("http://host:1234/path/to/servlet")
            .setTargetID("target")
            .setWidth(640)
            .setHeight(480);

        URL url = builder.build();
        assertNotNull(url);

        assertEquals(
            "http://host:1234/path/to/servlet?probeType=0&targetID=target&width=640&height=480&resolution=100",
            url.toExternalForm());
    }

    /**
     * Tests that building a chart URL which contains URL-encoded characters works correctly.
     */
    public void testBuildChartWithEscapesOk() throws Exception {
        ChartBuilder builder = new ChartBuilderImpl("http://host:1234/path/to/servlet")
            .setChartType("\u20ac")
            .setTargetID("\u20ac")
            .setWidth(640)
            .setHeight(480);

        URL url = builder.build();
        assertNotNull(url);

        assertEquals(
            "http://host:1234/path/to/servlet?probeType=0&targetID=%E2%82%AC&width=640&height=480&resolution=100&chartType=%E2%82%AC",
            url.toExternalForm());
    }
}
