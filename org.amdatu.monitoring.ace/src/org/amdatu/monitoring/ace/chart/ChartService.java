/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart;

/**
 * Provides a service for creating charts.
 */
public interface ChartService {

    /**
     * Creates a new builder for creating a chart.
     * 
     * @param targetId the identifier for the chart, cannot be <code>null</code> or empty.
     * @return a new {@link ChartBuilder} instance, never <code>null</code>.
     */
    ChartBuilder createChartBuilder();

    /**
     * Returns a name for this chart.
     * 
     * @return a descriptive (UI-friendly) name for this chart, never <code>null</code>.
     */
    String getName();

}
