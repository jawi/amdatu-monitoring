/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart;

/**
 * Factory service for creating new {@link ChartBuilder}s.
 * <p> 
 * {@link ChartService}s should make use of this factory to create new 
 * instances of {@link ChartBuilder}s for them. This allows them to create 
 * configurable URLs for charts without having to worry about how to obtain 
 * that configuration.
 * </p>
 */
public interface ChartBuilderFactory {

    /**
     * Creates a new chart builder.
     * 
     * @return a new {@link ChartBuilder} instance, never <code>null</code>.
     */
    ChartBuilder createBuilder();
}
