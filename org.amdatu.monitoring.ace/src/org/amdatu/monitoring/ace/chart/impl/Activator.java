/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart.impl;

import org.amdatu.monitoring.ace.chart.ChartBuilderFactory;
import org.amdatu.monitoring.ace.chart.ChartService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * Registers all chart services and the chart builder factory.
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setInterface(ChartBuilderFactory.class.getName(), null)
            .setImplementation(ChartBuilderFactoryImpl.class)
            .add(createConfigurationDependency()
                .setPid(ChartBuilderFactoryImpl.PID))
            );

        manager.add(createComponent()
            .setInterface(ChartService.class.getName(), null)
            .setImplementation(CpuChartService.class)
            .add(createServiceDependency()
                .setService(ChartBuilderFactory.class)
                .setRequired(true))
            );

        manager.add(createComponent()
            .setInterface(ChartService.class.getName(), null)
            .setImplementation(DiskChartService.class)
            .add(createServiceDependency()
                .setService(ChartBuilderFactory.class)
                .setRequired(true))
            );

        manager.add(createComponent()
            .setInterface(ChartService.class.getName(), null)
            .setImplementation(MemoryChartService.class)
            .add(createServiceDependency()
                .setService(ChartBuilderFactory.class)
                .setRequired(true))
            );

        manager.add(createComponent()
            .setInterface(ChartService.class.getName(), null)
            .setImplementation(NetworkChartService.class)
            .add(createServiceDependency()
                .setService(ChartBuilderFactory.class)
                .setRequired(true))
            );
    }
}
