/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart.impl;

import java.util.Dictionary;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.monitoring.ace.chart.ChartBuilder;
import org.amdatu.monitoring.ace.chart.ChartBuilderFactory;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * Provides a managed service implementation of {@link ChartBuilderFactory}.
 */
public class ChartBuilderFactoryImpl implements ChartBuilderFactory, ManagedService {
    
    public static final String PID = "org.amdatu.monitoring.ace.chart";
    
    private final AtomicReference<String> m_baseUrlRef = new AtomicReference<String>();

    @Override
    public void updated(Dictionary properties) throws ConfigurationException {
        String baseURL = null;
        
        if (properties != null) {
            baseURL = (String) properties.get("baseURL");
            if (baseURL == null) {
                throw new ConfigurationException("baseURL", "Missing or invalid value!");
            }
        }

        m_baseUrlRef.set(baseURL);
    }

    @Override
    public ChartBuilder createBuilder() {
        return new ChartBuilderImpl(m_baseUrlRef.get());
    }
}
