/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;

import org.amdatu.monitoring.ace.chart.ChartBuilder;

/**
 * Provides a base implementation for a {@link ChartBuilder}.
 */
final class ChartBuilderImpl implements ChartBuilder {

    private final String m_baseURL;

    private int m_width = 400;
    private int m_height = 300;
    private int m_resolution = 100;
    private long m_startDate = -1;
    private long m_endDate = -1;
    private String m_targetID;
    private int m_type;
    private String m_chartType;
    private String[] m_keys;
    private String[] m_options;

    /**
     * @param baseURL the base URL of the chart.
     */
    public ChartBuilderImpl(String baseURL) {
        m_baseURL = baseURL;
    }
    
    @Override
    public URL build() throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(m_baseURL).append("?");
        
        // All required options...
        sb.append("probeType=").append(urlEncode( m_type)).append("&");
        sb.append("targetID=").append(urlEncode(m_targetID)).append("&");
        sb.append("width=").append(urlEncode(m_width)).append("&");
        sb.append("height=").append(urlEncode(m_height));

        if (m_startDate > 0) {
            sb.append("&start=").append(urlEncode(m_startDate));
        }
        if (m_endDate > 0) {
            sb.append("&end=").append(urlEncode(m_endDate));
        }
        if (m_resolution > 0) {
            sb.append("&resolution=").append(urlEncode(m_resolution));
        }
        if (m_chartType != null) {
            sb.append("&chartType=").append(urlEncode(m_chartType));
        }
        if (m_keys != null) {
            for (String key : m_keys) {
                sb.append("&key=").append(urlEncode(key));
            }
        }
        if (m_options != null) {
            for (String option : m_options) {
                sb.append("&o=").append(urlEncode(option));
            }
        }

        return new URL(sb.toString());
    }

    @Override
    public ChartBuilder setChartType(String chartType) {
        m_chartType = chartType;
        return this;
    }

    @Override
    public ChartBuilder setEndDate(long endDate) {
        m_endDate = endDate;
        return this;
    }

    @Override
    public ChartBuilder setHeight(int height) {
        m_height = height;
        return this;
    }

    @Override
    public ChartBuilder setProbeType(int type) {
        m_type = type;
        return this;
    }

    @Override
    public ChartBuilder setResolution(int resolution) {
        m_resolution = resolution;
        return this;
    }
    
    @Override
    public ChartBuilder setShownProperties(String... names) {
        m_keys = Arrays.copyOf(names, names.length);
        return this;
    }

    @Override
    public ChartBuilder setStartDate(long startDate) {
        m_startDate = startDate;
        return this;
    }

    @Override
    public ChartBuilder setTargetID(String targetID) {
        m_targetID = targetID;
        return this;
    }

    @Override
    public ChartBuilder setWidth(int width) {
        m_width = width;
        return this;
    }
    
    private String urlEncode(Object value) {
        try {
            return URLEncoder.encode("" + value, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new InternalError("UTF-8 should always be supported!");
        }
    }
}
