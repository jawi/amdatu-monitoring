/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart.impl;

import org.amdatu.monitoring.ace.chart.ChartBuilder;
import org.amdatu.monitoring.ace.chart.ChartBuilderFactory;
import org.amdatu.monitoring.ace.chart.ChartService;

/**
 * Provides a base implementation for {@link ChartService}.
 */
abstract class BaseChartService implements ChartService {

    private final int m_type;
    
    private volatile ChartBuilderFactory m_builderFactory; 

    /**
     * Creates a new {@link BaseChartService} instance.
     * 
     * @param type the probe type to create the chart for, cannot be <code>null</code>.
     */
    protected BaseChartService(int type) {
        m_type = type;
    }

    @Override
    public final ChartBuilder createChartBuilder() {
        ChartBuilder builder = m_builderFactory.createBuilder();
        builder.setProbeType(m_type);
        return builder;
    }
}
