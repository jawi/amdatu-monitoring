/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.chart;

import java.io.IOException;
import java.net.URL;

import org.amdatu.monitoring.Probe;

/**
 * Provides a builder for creating new charts.
 */
public interface ChartBuilder {

    /**
     * Builds the URL for the chart.
     * 
     * @return a URL, never <code>null</code>.
     * @throws IOException in case of I/O problems creating the URL.
     */
    URL build() throws IOException;

    /**
     * Sets the type of chart to build.
     * 
     * @param chartType the type of chart, like "AreaChart" or "PieChart".
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setChartType(String chartType);

    /**
     * Sets the end data for the chart.
     * 
     * @param endDate a end date, in milliseconds (Epoch time).
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setEndDate(long endDate);

    /**
     * Sets the height of the chart.
     * 
     * @param height a height, in pixels.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setHeight(int height);

    /**
     * Sets the probe type for which to build the chart.
     * 
     * @param type a probe type, > 0.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setProbeType(int type);

    /**
     * Limits the number of data points to this given number.
     * 
     * @param resolution the number of data points to draw, or -1 to draw all datapoints.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setResolution(int resolution);

    /**
     * Sets the properties to show in the chart.
     * 
     * @param names an array with property keys, as present in the {@link Probe}s result.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setShownProperties(String... names);

    /**
     * Sets the starting date for the chart.
     * 
     * @param startDate a start date, in milliseconds (Epoch time).
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setStartDate(long startDate);

    /**
     * Sets the target ID to create the chart for.
     * 
     * @param targetID a target ID, cannot be <code>null</code> or empty.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setTargetID(String targetID);

    /**
     * Sets the width of the chart.
     * 
     * @param width a width, in pixels.
     * @return this builder, never <code>null</code>.
     */
    ChartBuilder setWidth(int width);
}