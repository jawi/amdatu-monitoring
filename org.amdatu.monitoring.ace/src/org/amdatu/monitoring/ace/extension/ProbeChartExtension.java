/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.extension;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.amdatu.monitoring.ace.chart.ChartBuilder;
import org.amdatu.monitoring.ace.chart.ChartService;
import org.apache.ace.client.repository.stateful.StatefulTargetObject;
import org.apache.ace.webui.NamedObject;
import org.apache.ace.webui.UIExtensionFactory;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * Provides an adapter service for registered {@link ChartService}s to be used by ACE in its WebUI.
 */
public class ProbeChartExtension implements UIExtensionFactory {

    private volatile ChartService m_chart;

    @Override
    public Component create(Map<String, Object> context) {
        StatefulTargetObject target = getStatefulTargetObject(context);

        VerticalLayout layout = new VerticalLayout();
        layout.setCaption(m_chart.getName());

        ChartBuilder builder = m_chart.createChartBuilder();
        builder.setTargetID(target.getID()).setWidth(400).setHeight(300);

        try {
            URL url = builder.build();
            if (url != null) {
                Embedded e = new Embedded(null, new ExternalResource(url));
                e.setType(Embedded.TYPE_BROWSER);
                e.setWidth("400px");
                e.setHeight("300px");
                layout.addComponent(e);
            }
            else {
                layout.addComponent(new Label("No chart data available."));
            }
        }
        catch (IOException e) {
            layout.addComponent(new Label(e.getMessage()));
            e.printStackTrace();
        }
        return layout;
    }

    /**
     * @param context
     */
    private StatefulTargetObject getStatefulTargetObject(Map<String, Object> context) {
        NamedObject no = (NamedObject) context.get("object");
        return (StatefulTargetObject) no.getObject();
    }
}
