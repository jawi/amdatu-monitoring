/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.monitor.impl;

import org.amdatu.monitoring.ace.monitor.Monitor;
import org.apache.ace.server.log.store.LogStore;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * Provides a bundle activator for registering a monitor for each registered probe.
 */
public class Activator extends DependencyActivatorBase {
    private static final String LOG_NAME = "monitoringlog";

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setInterface(Monitor.class.getName(), null)
            .setImplementation(LogStoreBackedMonitor.class)
            .add(createServiceDependency()
                .setService(LogStore.class, "(name=" + LOG_NAME + ")")
                .setRequired(true))
            );
    }

}
