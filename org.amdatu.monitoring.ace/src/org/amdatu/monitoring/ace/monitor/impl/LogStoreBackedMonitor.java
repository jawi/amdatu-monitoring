/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.monitoring.ace.monitor.impl;

import java.io.IOException;
import java.util.Dictionary;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.amdatu.monitoring.ace.monitor.Monitor;
import org.apache.ace.log.LogDescriptor;
import org.apache.ace.log.LogEvent;
import org.apache.ace.server.log.store.LogStore;

/**
 * Provides a {@link Monitor} implementation that is backed by a {@link LogStore} from ACE.
 */
public class LogStoreBackedMonitor implements Monitor {

    private volatile LogStore m_logStore;

    @Override
    public SortedMap<Long, Dictionary<String, Object>> getSamples(int probeType, String targetId) throws IOException {
        if (targetId == null || "".equals(targetId.trim())) {
            throw new IllegalArgumentException("TargetID cannot be null or empty!");
        }
        
        SortedMap<Long, Dictionary<String, Object>> result = new TreeMap<Long, Dictionary<String,Object>>();
        
        List<LogDescriptor> descriptors = m_logStore.getDescriptors(targetId);
        for (LogDescriptor descriptor : descriptors) {
            List<LogEvent> events = m_logStore.get(descriptor);
            for (LogEvent event : events) {
                if (event.getType() == probeType) {
                    result.put(event.getTime(), event.getProperties());
                }
            }
        }
        
        return result;
    }
}
