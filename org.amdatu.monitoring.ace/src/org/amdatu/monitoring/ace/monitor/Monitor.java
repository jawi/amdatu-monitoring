/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.monitor;

import java.io.IOException;
import java.util.Dictionary;
import java.util.SortedMap;

import org.amdatu.monitoring.Probe;

/**
 * Monitors a {@link Probe}, keeping a log of probe samples for further 
 * processing, such as creating a chart or histogram.
 */
public interface Monitor {

    /**
     * Returns a (sorted) map of probe values.
     * <p>
     * The keys in the returned map should represent the timestamps at which
     * the probe sample was taken. The value associated to the timestamp is
     * the raw probe data.
     * </p>
     * <p>
     * The number of stored samples should be limited to a maximum as to keep
     * memory usage within limits. The actual limit itself is up to the
     * implementation of a monitor.
     * </p>
     * 
     * @param probeType the type of the probe to retrieve the samples for;
     * @param targetID the identifier of the target to fetch the samples for, cannot be <code>null</code> or empty.
     * @return a map of probe samples, never <code>null</code>.
     * 
     * @throws IllegalArgumentException in case the given targetID was <code>null</code> or empty;
     * @throws IOException in case retrieving the samples failed.
     */
    SortedMap<Long, Dictionary<String, Object>> getSamples(int probeType, String targetID) throws IOException;
}
