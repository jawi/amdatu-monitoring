/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.logger;

import java.util.Dictionary;
import java.util.Properties;

import org.amdatu.monitoring.Probe;
import org.apache.ace.log.Log;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

/**
 * Registers the ACE logger functionality.
 */
public class Activator extends DependencyActivatorBase {
    
    private volatile ConfigurationAdmin m_configAdmin;
    private volatile BundleContext m_bundleContext;

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setImplementation(ProbeSampleLogService.class)
            .add(createServiceDependency()
                .setService(Probe.class)
                .setCallbacks("addProbe", "removeProbe")
                .setRequired(false))
            );

        manager.add(createComponent()
            .setImplementation(this)
            .add(createServiceDependency()
                .setService(ConfigurationAdmin.class)
                .setRequired(true))
            );
    }
    
    /**
     * Called by dependency manager upon start of this component.
     */
    final void start() throws Exception {
        if (m_bundleContext.getServiceReferences(Log.class.getName(), "(name=" + ProbeSampleLogService.LOG_NAME + ")") == null) {
            Dictionary properties = new Properties();
            properties.put("name", ProbeSampleLogService.LOG_NAME);
            
            m_configAdmin.createFactoryConfiguration("org.apache.ace.target.log.factory", null).update(properties);
            m_configAdmin.createFactoryConfiguration("org.apache.ace.target.log.store.factory", null).update(properties);
            m_configAdmin.createFactoryConfiguration("org.apache.ace.target.log.sync.factory", null).update(properties);

            Configuration config = m_configAdmin.getConfiguration("org.apache.ace.scheduler");
            properties = config.getProperties();
            properties.put(ProbeSampleLogService.LOG_NAME, "2000");
            config.update(properties);
        }
    }
}
