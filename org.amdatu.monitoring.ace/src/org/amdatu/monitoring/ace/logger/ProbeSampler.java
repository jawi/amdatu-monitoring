/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.logger;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicReference;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;
import org.apache.ace.log.Log;
import org.osgi.service.log.LogService;

/**
 * Takes periodically a sample from a probe and uploads it to a predefined log in ACE.
 */
public class ProbeSampler implements Runnable {

    private final int m_probeType;
    private final AtomicReference<Dictionary<String, Object>> m_lastRef;

    private volatile Probe m_probe;
    private volatile Log m_aceLog;
    private volatile LogService m_logService;

    /**
     * Creates a new {@link ProbeSampler} instance.
     * 
     * @param probeType the numeric identifier of the probe to log the samples for.
     */
    public ProbeSampler(int probeType) {
        m_probeType = probeType;
        m_lastRef = new AtomicReference<Dictionary<String, Object>>();
    }

    /**
     * Takes a sample from the contained probe and, if different from the last sample, logs it to ACE.
     */
    @Override
    public void run() {
        try {
            Dictionary<String, Object> newResult = m_probe.call();

            Dictionary<String, Object> oldResult = m_lastRef.getAndSet(newResult);
            // Only log changes...
            if ((oldResult != newResult) || !oldResult.equals(newResult)) {
                logSample(newResult);
            }
        }
        catch (Exception e) {
            m_logService.log(LogService.LOG_WARNING, "Failed to sample probe: #" + m_probeType, e);
        }
    }

    /**
     * Logs the given probe sample to ACE.
     * 
     * @param probeSample the probe sample to log, cannot be <code>null</code>.
     */
    private void logSample(Dictionary<String, Object> probeSample) {
        // ACE wants its logs to be filled with string values 
        // only, so we need to convert it first...
        m_aceLog.log(m_probeType, convertSample(probeSample));
    }

    /**
     * Converts the given probe sample into something that can be stored by ACE.
     * 
     * @param probeSample the probe sample to convert, cannot be <code>null</code>.
     * @return the converted probe sample, never <code>null</code>.
     */
    Dictionary<String, String> convertSample(Dictionary<String, Object> probeSample) {
        Dictionary<String, String> sample = new Hashtable<String, String>();

        Enumeration<String> keyEnum = probeSample.keys();
        while (keyEnum.hasMoreElements()) {
            String key = keyEnum.nextElement();
            Object value = probeSample.get(key);

            if (!Constants.KEY_PROBE_TYPE.equals(key)) {
                sample.put(key, value.toString());
            }
        }

        return sample;
    }
}
