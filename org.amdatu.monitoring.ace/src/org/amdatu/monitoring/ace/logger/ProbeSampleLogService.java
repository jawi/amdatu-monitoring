/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.logger;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.Probe;
import org.apache.ace.log.Log;
import org.apache.ace.scheduler.constants.SchedulerConstants;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

/**
 * Provides a service for logging the samples of all available probes to ACE.
 */
public class ProbeSampleLogService {
    static final String LOG_NAME = "monitoringlog";

    private final ConcurrentMap<String, Component> m_probeComponents = new ConcurrentHashMap<String, Component>();
    
    private volatile DependencyManager m_dm;

    /**
     * Adds a new probe to this service.
     * 
     * @param serviceRef the service reference of the probe, cannot be <code>null</code>;
     * @param probe the probe to add, can be <code>null</code>.
     */
    public void addProbe(ServiceReference serviceRef, Probe probe) {
        String probeName = (String) serviceRef.getProperty(Constants.KEY_PROBE_NAME);
        Integer probeType = (Integer) serviceRef.getProperty(Constants.KEY_PROBE_TYPE);

        if (probeName != null) {
            Component c = m_dm.createComponent().setInterface(
                Runnable.class.getName(), createConfig(probeName, "1000"))
                .setImplementation(new ProbeSampler(probeType))
                .add(m_dm.createServiceDependency()
                    .setService(Probe.class, "(name=" + probeName + ")")
                    .setRequired(true))
                .add(m_dm.createServiceDependency()
                    .setService(Log.class, "(name=" + LOG_NAME + ")")
                    .setRequired(true))
                .add(m_dm.createServiceDependency()
                    .setService(LogService.class)
                    .setRequired(false));
            m_dm.add(c);
            
            m_probeComponents.putIfAbsent(probeName, c);
        }
    }

    /**
     * Removes a probe from this service.
     * 
     * @param serviceRef the service reference of the probe, cannot be <code>null</code>;
     * @param probe the probe to remove, can be <code>null</code>.
     */
    public void removeProbe(ServiceReference serviceRef, Probe probe) {
        String probeName = (String) serviceRef.getProperty(Constants.KEY_PROBE_NAME);
        
        Component oldComponent = m_probeComponents.remove(probeName);
        if (oldComponent != null) {
            m_dm.remove(oldComponent);
        }
    }
    
    private Dictionary<String, String> createConfig(String type, String interval) {
        Dictionary<String, String> properties = new Hashtable<String, String>();
        properties.put(SchedulerConstants.SCHEDULER_DESCRIPTION_KEY, "Monitoring Task: " + type);
        properties.put(SchedulerConstants.SCHEDULER_NAME_KEY, "monitoring-" + type);
        properties.put(SchedulerConstants.SCHEDULER_RECIPE, interval);
        return properties;
    }
}
