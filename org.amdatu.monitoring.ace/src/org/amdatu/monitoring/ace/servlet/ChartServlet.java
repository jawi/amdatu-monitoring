/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.monitoring.ace.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.monitoring.Constants;
import org.amdatu.monitoring.ace.monitor.Monitor;

/**
 * Provides a servlet for creating actual charts, based on Google Charts.
 */
public class ChartServlet extends HttpServlet {

    /**
     * Small flyweight object for passing around the various parameters of this servlet.
     */
    public static class Parameters {
        String m_targetId;
        String m_chartType;
        String[] m_keys;
        String[] m_options;
        int m_probeType;
        int m_width;
        int m_height;
        int m_resolution = -1;
        long m_start = -1L;
        long m_end = Long.MAX_VALUE;
    }

    // Injected by dependency manager...
    private volatile Monitor m_monitor;

    /**
     * @param params
     * @return
     * @throws IOException
     */
    String createChartHtml(Map<Long, Dictionary<String, Object>> samples, Parameters params) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>");
        sb.append("<script type=\"text/javascript\">");
        sb.append("google.load('visualization', '1.0', {'packages':['corechart']});");
        sb.append("google.setOnLoadCallback(dc);");
        sb.append("function dc() {");
        sb.append("var d = new google.visualization.DataTable(");
        // insert sample data here...
        jsonifySampleData(sb, samples, params);
        sb.append(");");
        sb.append("var o=");
        jsonifyOptions(sb, params);
        sb.append(";");
        sb.append("var ac = new google.visualization.").append(params.m_chartType)
            .append("(document.getElementById('ch'));");
        sb.append("ac.draw(d, o);");
        sb.append("}");
        sb.append("</script></head><body><div id=\"ch\"></div></body></html>");
        String html = sb.toString();
        return html;
    }

    /**
     * Writes out a JSON-ish map for the parameters.
     * 
     * @param sb the string builder to write to;
     * @param params the parameters for the chart.
     */
    void jsonifyOptions(StringBuilder sb, Parameters params) {
        sb.append("{");
        sb.append("hAxis:{title:'Time'}");

        if (params.m_width > 0) {
            sb.append(",'width':").append(params.m_width);
        }
        if (params.m_height > 0) {
            sb.append(",'height':").append(params.m_height);
        }
        if (params.m_options != null) {
            for (String option : params.m_options) {
                sb.append(",");
                sb.append(option);
            }
        }

        sb.append("}");
    }

    /**
     * Writes out a JSON-ish array for the given sample data.
     * 
     * @param sb the string builder to write to;
     * @param samples the sample data to JSONify;
     * @param params the parameters for the chart.
     */
    void jsonifySampleData(StringBuilder sb, Map<Long, Dictionary<String, Object>> samples, Parameters params) {
        List<String> keys = new ArrayList<String>();
        if (params.m_keys != null) {
            keys.addAll(Arrays.asList(params.m_keys));
        }

        boolean headerWritten = false;

        List<Long> timestamps = new ArrayList<Long>(samples.keySet());
        if (params.m_resolution > 0) {
            // Take only the N last samples...
            int startIdx = Math.max(0, timestamps.size() - params.m_resolution);
            int endIdx = timestamps.size();

            timestamps = timestamps.subList(startIdx, endIdx);
        }

        sb.append('{');
        for (Long time : timestamps) {
            if (time >= params.m_start && time <= params.m_end) {
                Dictionary<String, Object> sampleData = samples.get(time);

                if (!headerWritten) {
                    headerWritten = true;

                    if (keys.isEmpty()) {
                        // Dynamically gather all keys from the supplied sample data...
                        Enumeration<String> keyEnum = sampleData.keys();
                        while (keyEnum.hasMoreElements()) {
                            String key = keyEnum.nextElement();
                            if (Constants.KEY_PROBE_TYPE.equals(key)) {
                                continue;
                            }
                            keys.add(key);
                        }
                    }

                    sb.append("cols: [{label:'Time', type:'date'}");
                    for (String key : keys) {
                        Object value = sampleData.get(key);

                        sb.append(",{label:'").append(key).append("', type:'").append(getJsType(value)).append("'}");
                    }
                    sb.append("], rows: [");
                }
                else {
                    sb.append(",");
                }

                sb.append("{c:[{v: new Date(").append(time).append(")}");

                for (String key : keys) {
                    Object value = sampleData.get(key);

                    sb.append(",{v: ").append(value).append("}");
                }

                sb.append("]}");
            }
        }
        if (headerWritten) {
            // close the rows array...
            sb.append("]");
        }
        sb.append("}");
    }

    /**
     * @param monitor the monitor to set
     */
    void setMonitor(Monitor monitor) {
        m_monitor = monitor;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Parameters params = parseParameters(req);
        if (params == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Map<Long, Dictionary<String, Object>> samples = m_monitor.getSamples(params.m_probeType, params.m_targetId);
        if (samples == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        String html = createChartHtml(samples, params);

        resp.setContentType("text/html");
        resp.setHeader("Cache-Control", "no-cache");

        PrintWriter writer = resp.getWriter();
        try {
            writer.write(html);
        }
        finally {
            writer.flush();
        }
    }

    /**
     * Tries to determine what corresponding JavaScript type the given value has.
     * 
     * @param value the value to check, should not be <code>null</code>.
     * @return the JavaScript equivalent type name, never <code>null</code>.
     */
    private String getJsType(Object value) {
        if (isNumeric(value)) {
            return "number";
        }
        else if (value instanceof Boolean) {
            return "boolean";
        }
        return "string";
    }

    /**
     * Tries to determine whether the given value is a number in any kind of form (that is: string, or an actual numeric value).
     * 
     * @param value the valuer to test, cannot be <code>null</code>.
     * @return <code>true</code> if the given value represents a numeric value, <code>false</code> otherwise.
     */
    private boolean isNumeric(Object value) {
        if (value instanceof Number) {
            return true;
        }

        String str = String.valueOf(value);

        try {
            Long.parseLong(str);
            return true;
        }
        catch (NumberFormatException e) {
            // Ignore; not a long number...
        }

        try {
            Double.parseDouble(str);
            return true;
        }
        catch (NumberFormatException e) {
            // Ignore; not a long number...
        }

        return false;
    }

    /**
     * Parses the arguments from the given servlet request.
     * 
     * @param req the servlet request to parse the parameters from, cannot be <code>null</code>.
     * @return the parsed parameters, never <code>null</code>.
     */
    private Parameters parseParameters(HttpServletRequest req) {
        Parameters params = new Parameters();

        params.m_targetId = req.getParameter("targetID");
        if (params.m_targetId == null || "".equals(params.m_targetId.trim())) {
            return null;
        }

        params.m_chartType = req.getParameter("chartType");
        if (params.m_chartType == null || "".equals(params.m_chartType.trim())) {
            params.m_chartType = "LineChart";
        }

        String[] keys = req.getParameterValues("key");
        if (keys != null) {
            params.m_keys = keys;
        }

        String[] options = req.getParameterValues("o");
        if (options != null) {
            params.m_options = options;
        }

        try {
            String type = req.getParameter("probeType");
            params.m_probeType = Integer.parseInt(type);
            if (params.m_probeType < 1) {
                return null;
            }
        }
        catch (NumberFormatException exception) {
            return null;
        }

        try {
            params.m_width = Integer.parseInt(req.getParameter("width"));
        }
        catch (NumberFormatException exception) {
            return null;
        }

        try {
            params.m_height = Integer.parseInt(req.getParameter("height"));
        }
        catch (NumberFormatException exception) {
            return null;
        }

        try {
            params.m_start = Long.parseLong(req.getParameter("start"));
        }
        catch (NumberFormatException exception) {
            // Ignore; start timestamp is optional...
        }

        try {
            params.m_end = Long.parseLong(req.getParameter("end"));
        }
        catch (NumberFormatException e) {
            // Ignore; end timestamp is optional...
        }

        try {
            params.m_resolution = Integer.parseInt(req.getParameter("resolution"));
        }
        catch (NumberFormatException e) {
            // Ignore; resolution is optional...
        }

        return params;
    }
}
