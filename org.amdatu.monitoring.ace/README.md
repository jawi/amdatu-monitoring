# Amdatu Monitoring - ACE integration

This project provides ACE integration for the Amdatu monitoring project, allowing targets
to be probed and see their sampled results in the ACE web UI.


## Overview

### org.amdatu.monitoring.ace.chart

This bundle provides the actual charts for the various probes. 

### org.amdatu.monitoring.ace.monitor

This bundle gathers probe samples, and make them available for charts to be used.

### org.amdatu.monitoring.ace.servlet

This bundle provides a small servlet that is used to generate the actual HTML for the 
charts to be displayed.

### org.amdatu.monitoring.ace.ui

This bundle provides an extension point for targets and for each probe add a new tab to
the target popup in ACE with the actual chart data.

### org.amdatu.monitoring.ace.logger

This bundle takes samples from probes at the target and logs them to ACE.  


## Deployment

On the ACE server, the following bundles should be deployed (in <tt>ace-bundles</tt>):

 * org.amdatu.monitoring.api
 * org.amdatu.monitoring.ace.ui
 * org.amdatu.monitoring.ace.chart
 * org.amdatu.monitoring.ace.monitor
 * org.amdatu.monitoring.ace.servlet
 
On the ACE target (management agent), the following bundles should be deployed (added to
the classpath, and started with the <tt>bundle=...</tt> option):
 
 * org.amdatu.monitoring.api
 * org.amdatu.monitoring.{cpu|disk|...}
 * org.amdatu.monitoring.ace.logger
 * org.apache.felix.dependencymanager 

## License

This project is licensed under the Apache License, Version 2.0.
